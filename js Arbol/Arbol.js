class Arbol{
    constructor(){
        this.nodoPadre = this.aNPadre();
    }
    agNodo(Tipo, valor, nivel, padre, hijoI = 0, hijoD = 0){
        var nodo = new Nodo(Tipo,valor,nivel,padre,hijoI,hijoD);
        return nodo;
    }
    aNPadre(){
        var nodo = new Nodo("i",  0,  0, 0,  1, 1);
        return nodo;
    }
}