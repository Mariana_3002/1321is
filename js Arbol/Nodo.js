class Nodo{
    constructor(Tipo, valor, nivel, padre, hijoI = 0, hijoD = 0){
        this.Tipo = Tipo;
        this.valor = valor;
        this.nivel = nivel;
        this.padre = padre;
        this.hijoD = hijoI;
        this.hijoD = hijoD;
        
        if(hijoI == 1)
        this.hijoI = this.cHijo(Tipo = "i", valor = 0, nivel = nivel +1, this.nivel);

        if(hijoD == 1)
        this.hijoD = this.cHijo(Tipo = "d", valor = 0, nivel = nivel + 1, this.nivel);

    }
    cHijo (Tipo, valor, nivel, padre){
        var cHijo = new Nodo(Tipo, valor, nivel, padre);
        return cHijo;
    }
}